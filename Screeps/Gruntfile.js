module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-screeps');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-run');
    grunt.loadNpmTasks('grunt-git');
    grunt.loadNpmTasks('grunt-banner');

    grunt.initConfig({
        screeps: {
            options: {
                email: process.env.SCREEPS_MAIL,
                password: process.env.SCREEPS_PASS,
                branch: 'default',
                ptr: false
            },
            dist: {
                src: ['src/*']
            }
        },
        uglify: {
            options: {
                banner: '/*\n<%= pkg.name %> v<%= pkg.version %> <%= grunt.template.today("dd.mm.yyyy HH:MM") %>\n<%= pkg.description %>\nAuthor: <%= pkg.author %>\n*/\n'
            },
            build: {
                src: 'src/*.js',
                dest: 'dist/main.js'
            }
        },
        run: {
            patch: {
                exec: 'npm version patch',
            },
            minor: {
                exec: 'npm version minor',
            },
            major: {
                exec: 'npm version major',
            },
            prepatch: {
                exec: 'npm version prepatch',
            },
            preminor: {
                exec: 'npm version preminor',
            },
            premajor: {
                exec: 'npm version premajor',
            },
            build: {
                exec: 'npm version prerelease',
            }
        },
        gitadd: {
            task: {
                options: {
                    all: true
                },
                files: {
                    src: ['.']
                }
            }
        },
        gitcommit: {
            screeps: {
                options: {
                    message: 'v<%= pkg.version %>',
                    allowEmpty: false
                },
                files: {
                    src: ['.']
                }
            }
        },
        gitpush: {
            files: {
                options: {
                    all: true
                }
            },
            tags: {
                options: {
                    tags: true
                }
            }
        },
        gittag: {
            screeps: {
                options: {
                    tag: 'v<%= pkg.version %>'
                }
            }
        },
        usebanner: {
            jsFiles: {
                options: {
                    position: 'top',
                    banner: '/*\n<%= pkg.name %> v<%= pkg.version %> <%= grunt.template.today("dd.mm.yyyy HH:MM") %>\n<%= pkg.description %>\nCopyright <%= pkg.author %> <%= grunt.template.today("yyyy") %>\n*/',
                    linebreak: true,
                    replace: true
                },
                files: {
                    src: ['src/*.js']
                }
            }
        },
    });
    grunt.registerTask('readpkg', 'Read in the package.json file', function () {

        grunt.config.set('pkg', grunt.file.readJSON('./package.json'));

    });
    grunt.registerTask('default', ['remote']);
    grunt.registerTask('patch', ['run:patch', 'readpkg', 'usebanner', 'gitadd', 'gitcommit', 'gittag', 'gitpush']);
    grunt.registerTask('minor', ['run:minor', 'readpkg', 'usebanner', 'gitadd', 'gitcommit', 'gittag', 'gitpush']);
    grunt.registerTask('major', ['run:major', 'readpkg', 'usebanner', 'gitadd', 'gitcommit', 'gittag', 'gitpush']);
    grunt.registerTask('prepatch', ['run:prepatch', 'readpkg', 'usebanner', 'gitadd', 'gitcommit', 'gitpush']);
    grunt.registerTask('preminor', ['run:preminor', 'readpkg', 'usebanner', 'gitadd', 'gitcommit', 'gitpush']);
    grunt.registerTask('premajor', ['run:premajor', 'readpkg', 'usebanner', 'gitadd', 'gitcommit', 'gitpush']);
    grunt.registerTask('build', ['run:build', 'readpkg', 'usebanner', 'gitadd', 'gitcommit', 'gitpush']);

};