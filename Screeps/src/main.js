/*
screeps_scripte v2.5.12 04.01.2020 00:14

Copyright Sven Nolting 2020
*/

require('prototype.spawn')();
let roleHarvester = require('role.harvester');
let roleLdh = require('role.ldh');
let roleUpgrader = require('role.upgrader');
let roleBuilder = require('role.builder');
let roleRepairer = require('role.repairer');
let roleTower = require('role.tower');
let roleLorry = require('role.lorry');

let minHarvesters;
let minLdhs;
let minLorries;
let minUpgraders;
let minBuilders;
let minRepairer;

function spawnManager(spawn) {
    let numberOfHarvesters = _.sum(Game.creeps, (c) => c.memory.role == 'harvester');
    let numberOfLdhs = _.sum(Game.creeps, (c) => c.memory.role == 'ldh');
    let numberOfUpgraders = _.sum(Game.creeps, (c) => c.memory.role == 'upgrader');
    let numberOfBuilders = _.sum(Game.creeps, (c) => c.memory.role == 'builder');
    let numberOfRepairer = _.sum(Game.creeps, (c) => c.memory.role == 'repairer');
    let numberOfLorries = _.sum(Game.creeps, (c) => c.memory.role == 'lorry');
    let name;
    let energy = spawn.room.energyCapacityAvailable;

    let storages = spawn.room.find(FIND_STRUCTURES, {
        filter: (structure) => {
            return [STRUCTURE_STORAGE].includes(structure.structureType) && structure.isActive() && structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
        }
    });

    if (storages.length > 0) {
        minHarvesters *= 3;
    }

    if (numberOfHarvesters < minHarvesters) {
        name = spawn.spawnCustomCreep(energy, 'harvester');

        if (name === ERR_NOT_ENOUGH_ENERGY) {
            spawn.spawnCustomCreep(spawn.room.energyAvailable, 'harvester');
        }
    } else if (numberOfBuilders < minBuilders) {
        name = spawn.spawnCustomCreep(energy, 'builder');
    } else if (numberOfLorries < minLorries && storages.length > 0) {
        name = spawn.spawnLorry(energy);
    } else if (numberOfUpgraders < minUpgraders) {
        name = spawn.spawnCustomCreep(energy, 'upgrader');
    } else if (numberOfLdhs < minLdhs) {
        name = spawn.spawnLongDistanceHarvester(energy, Math.floor(energy / 200), spawn.room.name, 'W23N27')
    } else if (numberOfRepairer < minRepairer) {
        name = spawn.spawnCustomCreep(energy, 'repairer');
    }

    if (name !== undefined && Game.creeps[name] !== undefined) {
        console.log('Spawning new ' + Game.creeps[name].memory.role + ' creep: ' + name)
    }
}

function roadManager(spawn) {
    const path = spawn.room.findPath(spawn.pos, spawn.room.controller.pos, {
        ignoreCreeps: true,
        ignoreRoads: true,
        swampCost: 1
    });
    path.forEach(function (pathPos) {
        if (pathPos.x !== spawn.room.controller.pos.x && pathPos.y !== spawn.room.controller.pos.y)
            spawn.room.createConstructionSite(pathPos.x, pathPos.y, STRUCTURE_ROAD);
    });
}

function flagManager(spawn) {
    for (let flag in Game.flags) {
        if (Game.flags[flag].color === COLOR_GREEN && spawn.room.controller.level >= 2) {
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_RAMPART);
            Game.flags[flag].remove();
        } else if (Game.flags[flag].color === COLOR_GREEN) {
            spawn.room.visual.text("Ramppart", Game.flags[flag].pos.x, Game.flags[flag].pos.y - 1, {align: "center"});
        }
        if (Game.flags[flag].color === COLOR_ORANGE && spawn.room.controller.level >= 3 && Game.flags[flag].room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType === STRUCTURE_TOWER)
            }
        }).length < 1) {
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_TOWER);
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_RAMPART);
            Game.flags[flag].remove();
        } else if (Game.flags[flag].color === COLOR_ORANGE && spawn.room.controller.level >= 5 && Game.flags[flag].room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType === STRUCTURE_TOWER)
            }
        }).length < 2) {
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_TOWER);
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_RAMPART);
            Game.flags[flag].remove();
        } else if (Game.flags[flag].color === COLOR_ORANGE && spawn.room.controller.level >= 7 && Game.flags[flag].room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType === STRUCTURE_TOWER)
            }
        }).length < 3) {
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_TOWER);
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_RAMPART);
            Game.flags[flag].remove();
        } else if (Game.flags[flag].color === COLOR_ORANGE && spawn.room.controller.level >= 8 && Game.flags[flag].room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType === STRUCTURE_TOWER)
            }
        }).length < 6) {
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_TOWER);
            spawn.room.createConstructionSite(Game.flags[flag].pos, STRUCTURE_RAMPART);
            Game.flags[flag].remove();
        } else if (Game.flags[flag].color === COLOR_ORANGE) {
            spawn.room.visual.text("Tower", Game.flags[flag].pos.x, Game.flags[flag].pos.y - 1, {align: "center"});
        }

    }
}

module.exports.loop = function () {
    // check for memory entries of died creeps by iterating over Memory.creeps
    for (let name in Memory.creeps) {
        // and checking if the creep is still alive
        if (Game.creeps[name] === undefined) {
            // if not, delete the memory entry
            delete Memory.creeps[name];
            console.log("Removed creep " + name + " from Memory");
        }
    }

    var spawn = Game.spawns['Spawn1'];
    
    minHarvesters = 2;
    minLdhs = 3;
    minLorries = 1;
    minUpgraders = 2;
    minBuilders = 1;
    minRepairer = 5;

    spawnManager(spawn);
    roadManager(spawn);
    flagManager(spawn);

    if (Game.time % 500 === 0) {
        const extensions = spawn.room.find(FIND_MY_STRUCTURES, {
            filter: {structureType: STRUCTURE_EXTENSION}
        });
        console.log('Spawn has ' + extensions.length + ' extensions available');


    }
    var numberOfHarvesters = _.sum(Game.creeps, (c) => c.memory.role == 'harvester');
    var numberOfLdhs = _.sum(Game.creeps, (c) => c.memory.role == 'ldh');
    var numberOfUpgraders = _.sum(Game.creeps, (c) => c.memory.role == 'upgrader');
    var numberOfBuilders = _.sum(Game.creeps, (c) => c.memory.role == 'builder');
    var numberOfRepairer = _.sum(Game.creeps, (c) => c.memory.role == 'repairer');
    var numberOfLorries = _.sum(Game.creeps, (c) => c.memory.role == 'lorry');

    spawn.room.visual.text("Harvester:\t" + numberOfHarvesters + "/" + minHarvesters, 0, 0, {align: 'left'});
    spawn.room.visual.text("LDH:\t\t" + numberOfLdhs + "/" + minLdhs, 0, 1, {align: 'left'});
    spawn.room.visual.text("Upgrader:\t" + numberOfUpgraders + "/" + minUpgraders, 0, 2, {align: 'left'});
    spawn.room.visual.text("Builder:\t" + numberOfBuilders + "/" + minBuilders, 0, 3, {align: 'left'});
    spawn.room.visual.text("Repairer:\t" + numberOfRepairer + "/" + minRepairer, 0, 4, {align: 'left'});
    spawn.room.visual.text("Lorries:\t" + numberOfLorries + "/" + minLorries, 0, 5, {align: 'left'});
    spawn.room.visual.text("Total:\t" + (numberOfHarvesters + numberOfLdhs + numberOfUpgraders + numberOfBuilders + numberOfRepairer + numberOfLorries), 0, 6, {align: 'left'});

    if (spawn.spawning) {
        var spawningCreep = Game.creeps[spawn.spawning.name];
        spawn.room.visual.text(
            '🛠️' + spawningCreep.memory.role,
            spawn.pos.x + 1,
            spawn.pos.y,
            {align: 'left', opacity: 0.8});
    }

    towers = spawn.room.find(FIND_MY_STRUCTURES, {
        filter: (structure) => structure.structureType === STRUCTURE_TOWER
    });
    towers.forEach(tower => {
        roleTower.run(tower);
    });

    if (spawn.room.controller.safeModeAvailable > 0 && spawn.room.controller.safeModeCooldown === undefined && spawn.room.controller.safeMode === undefined) spawn.room.controller.activateSafeMode();

    for (var name in Game.creeps) {
        var creep = Game.creeps[name];
        if (creep.memory.role === 'harvester') {
            roleHarvester.run(creep);
        }
        if (creep.memory.role === 'upgrader') {
            roleUpgrader.run(creep);
        }
        if (creep.memory.role === 'builder') {
            roleBuilder.run(creep);
        }
        if (creep.memory.role === 'repairer') {
            roleRepairer.run(creep);
        }
        if (creep.memory.role === 'ldh') {
            roleLdh.run(creep);
        }
        if (creep.memory.role === 'lorry') {
            roleLorry.run(creep);
        }
    }
};
