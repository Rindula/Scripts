/*
screeps_scripte v2.5.12 04.01.2020 00:14

Copyright Sven Nolting 2020
*/

module.exports = function () {
    StructureSpawn.prototype.spawnCustomCreep = function (energy, roleName) {
        var numberOfParts = Math.floor(energy/(BODYPART_COST.work + BODYPART_COST.carry + BODYPART_COST.move));
        var body = [];

        for (let i = 0; i < numberOfParts; i++) {
            body.push(WORK);
        }
        for (let i = 0; i < numberOfParts; i++) {
            body.push(CARRY);
        }
        for (let i = 0; i < numberOfParts; i++) {
            body.push(MOVE);
        }

        return this.spawnCreep(body, roleName + Game.time, {memory: {role: roleName, working: false}});
    };

    StructureSpawn.prototype.spawnLongDistanceHarvester = function (energy, numberOfWorkParts, home, target) {
        var body = [];
        for (let i = 0; i < numberOfWorkParts; i++) {
            body.push(WORK);
        }
        energy -= 150 * numberOfWorkParts;

        var numberOfParts = Math.floor(energy/100);
        for (let i = 0; i < numberOfParts; i++) {
            body.push(CARRY);
        }
        for (let i = 0; i < numberOfParts + numberOfWorkParts; i++) {
            body.push(MOVE);
        }
        return this.spawnCreep(body, 'ldh' + Game.time, {memory: {role: 'ldh', home: home, target: target, working: false}});
    };

    StructureSpawn.prototype.spawnLorry = function (energy)
    {
        body = [];
        numberOfParts = Math.floor(energy/(BODYPART_COST.carry + BODYPART_COST.move));
        for (let i = 0; i < numberOfParts; i++) {
            body.push(CARRY);
            body.push(MOVE);
        }
        return this.spawnCreep(body, 'lorry' + Game.time, {memory: {role: 'lorry', working: false}});
    }
};