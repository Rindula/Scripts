/*
screeps_scripte v2.5.12 04.01.2020 00:14

Copyright Sven Nolting 2020
*/

var roleLdh = {

    /** @param {Creep} creep **/
    run: function (creep) {

        if (creep.memory.working && creep.store.getFreeCapacity() === 0) {
            creep.memory.working = false;
            creep.say('Going home');
        }
        if (!creep.memory.working && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.working = true;
            creep.say('🔄 harvest');
        }

        if (creep.memory.working) {
            if (creep.room.name === creep.memory.target) {
                const source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);

                if (source !== undefined && source !== null) {
                    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                        creep.moveTo(source, {visualizePathStyle: {stroke: '#ffaa00'}});
                    }
                } else {
                    creep.moveTo(24, 24);
                }
            } else {
                const exit = creep.room.findExitTo(creep.memory.target);
                creep.moveTo(creep.pos.findClosestByRange(exit));
            }
        } else {
            if (creep.room.name === creep.memory.home) {
                let targets = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return [STRUCTURE_STORAGE].includes(structure.structureType) && structure.isActive() && structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
                    }
                });
                if (targets.length === 0) {
                    targets = creep.room.find(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return (structure.structureType === STRUCTURE_SPAWN || structure.structureType === STRUCTURE_EXTENSION) && structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
                        }
                    });
                }
                if (targets.length > 0) {
                    if (creep.transfer(targets[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                        creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            } else {
                const exit = creep.room.findExitTo(creep.memory.home);
                creep.moveTo(creep.pos.findClosestByRange(exit));
            }
        }
    }
};

module.exports = roleLdh;
