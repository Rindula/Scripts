/*
screeps_scripte v2.5.12 04.01.2020 00:14

Copyright Sven Nolting 2020
*/

var roleTower = {

    /** @param {STRUCTURE_TOWER} tower */
    run: function (tower) {
        if (tower) {
            const closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => structure.hits < structure.hitsMax && structure.structureType !== STRUCTURE_WALL
            });
            if (closestDamagedStructure) {
                tower.repair(closestDamagedStructure);
            }

            const closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            if (closestHostile) {
                tower.attack(closestHostile);
            }
        }
    }
};

module.exports = roleTower;