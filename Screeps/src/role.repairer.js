/*
screeps_scripte v2.5.12 04.01.2020 00:14

Copyright Sven Nolting 2020
*/

var roleBuilder = require('role.builder');
var roleRepairer = {

    /** @param {Creep} creep **/
    run: function (creep) {

        if (creep.memory.working && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.working = false;
        }
        if (!creep.memory.working && creep.store.getFreeCapacity() === 0) {
            creep.memory.working = true;
        }

        if (creep.memory.working) {
            const target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) => s.hits < s.hitsMax
            });
            if (target !== undefined && target !== null) {
                if (creep.repair(target) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ff0000'}});
                }
            } else {
                roleBuilder.run(creep);
            }
        } else {
            require('functions').getToEnergySource(creep);
        }
    }
};

module.exports = roleRepairer;