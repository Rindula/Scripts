/*
screeps_scripte v2.5.12 04.01.2020 00:14

Copyright Sven Nolting 2020
*/

var roleHarvester = require('role.harvester');
var roleLorry = {

    /** @param {Creep} creep **/
    run: function (creep) {

        if (creep.memory.working && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.working = false;
            creep.say('🛢 receive');
        }
        if (!creep.memory.working && creep.store[RESOURCE_ENERGY] > 0) {
            creep.memory.working = true;
            creep.say('🚚 deliver');
        }

        if (creep.memory.working) {
            const target = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                filter: (s) => (s.structureType === STRUCTURE_TOWER || s.structureType === STRUCTURE_SPAWN || s.structureType === STRUCTURE_EXTENSION)
                    && s.store.getFreeCapacity(RESOURCE_ENERGY) > 0
            });
            if (target !== undefined || target !== null) {
                if (creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            }
        } else {
            const target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) => (s.structureType === STRUCTURE_CONTAINER || s.structureType === STRUCTURE_STORAGE) && (s.store.getUsedCapacity(RESOURCE_ENERGY) > 0)
            });
            if (target !== undefined && target !== null) {
                if (creep.withdraw(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ff0000'}});
                }
            } else {
                roleHarvester.run(creep);
            }
        }
    }
};

module.exports = roleLorry;