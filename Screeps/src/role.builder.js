/*
screeps_scripte v2.5.12 04.01.2020 00:14

Copyright Sven Nolting 2020
*/

var roleUpgrader = require('role.upgrader');
var roleBuilder = {

    /** @param {Creep} creep **/
    run: function (creep) {

        if (creep.memory.working && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.working = false;
            creep.say('🔄 harvest');
        }
        if (!creep.memory.working && creep.store.getFreeCapacity() === 0) {
            creep.memory.working = true;
            creep.say('🚧 build');
        }

        if (creep.memory.working) {
            const targetStorage = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES, {
                filter: (structure) => {
                    return (structure.structureType === STRUCTURE_STORAGE);
                }
            });
            const target = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES);
            if (targetStorage !== undefined && targetStorage !== null) {
                if (creep.build(targetStorage) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(targetStorage, {visualizePathStyle: {stroke: '#ffffff'}});
                }
            } else if (target !== undefined && target !== null) {
                if (creep.build(target) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
                }
            } else {
                roleUpgrader.run(creep);
            }
        } else {
            require('functions').getToEnergySource(creep);
        }
    }
};

module.exports = roleBuilder;
